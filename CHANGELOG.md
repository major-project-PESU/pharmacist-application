# Changelog

## v0.0.4 -> v0.0.5

* Added : Registration, Login page
* Added : Providers to validate login and register
* Added : Google Maps support while registration
* Modified : Bifurcation of registeration instead of single page
* WIP : Bifurcating location to city-level instead of co-ordinate level

## v0.0.5 -> v0.0.6

* Fixed : Cordova Dependency issues / Gradle build issues for Android Platform
* Added : Reverse Geocoding based on Location using gMaps and Ionic Gecoder Native plugin
* Added : Auto-fill address based on location
* Added : Drag event on the marker triggers auto-fill location
* Added : City, Locality (if applicable) and PIN code as separate options to complete address
* Added : Provider for API integration, error handling in case of incorrect login attempt

## v0.0.6 -> v0.0.7

* Added : Bidding Provider support for Pharmacist to get Unseen Bids, Active Bids (bids that they have sent out, but aren't choosen by the patient yet) and for a pharmacist to accept an order by quoting a price for the order
* Added : Active Bids Page to show a list of bids that aren't confimred by the patient but pharmacist has provided a proforma quote for the request
* Added : Page to check medicine list in request and to quote a price.
* Modified : Bids page to view request medicine list and quote price before accepting an order
.
* Modified : Landing Page to accomodate an extra tab that helps pharmacist keep track of pending orders (Unseen Order Requests)

## v0.0.7 -> v0.0.8

* Added : Order provider to interact with API in order to get Pharmacist Orders (Active/Past) and Change Order status (w.r.t Order ID)
* Added : Order Details page for pharmacist to check order summary
* Added : Change Status Modal to allow pharmacist to change status of an order
* Fixed : Authentication Hook, used JWT to automatically login if a valid JWT token is detected.
* Modified : Order Page now gets data from the API instead of hardcoded data, added buttons to view summary and change status

## v0.0.8 -> v0.0.9

* Added : Provider functions for uploading an image-based request to pharmacies
* Added : Image Invoice Page to generate invoice by adding medicines when an image of a prescription is recieved
* Modified : From Pending Bids, redirection to a page where bidding is done is done differently for Image-based request vs Cart-based request
* Modified : Landing Page has 4 tabs instead of 5, removed Active Tab
* Modified : Active Bids moved to Profile


## v0.0.9 -> v0.0.10

* Modified : UI color scheme for all pages.
* Modified : Alignment of design components.

## v0.0.10 -> v1.0.0

* Added : Possibility to filter order between active and past
* Added : Form validation, Date validation and Confirm alert to image-invoice page and checkout-invoice page
* Added : Option to indicate urgent order in the bids page 
* Added : General alerts wherever necessary
* Modified : Active bids removed from bottom navigation bar and shifted to profiles tab
* Fixed : Can't change order status on past orders anymore