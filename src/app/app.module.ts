import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { RegisterPage } from '../pages/register/register';
import { LandingPage } from '../pages/landing/landing';
import { BidsPage } from '../pages/bids/bids';
import { OrdersPage } from '../pages/orders/orders';
import { ViewRequestPage } from '../pages/view-request/view-request';
import { Register_2Page } from '../pages/register-2/register-2';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { AuthProvider } from '../providers/auth/auth';
import { HttpModule } from '@angular/http';
import { BiddingProvider } from '../providers/bidding/bidding';
import { ActivebidsPage } from '../pages/activebids/activebids';
import { CheckoutInvoicePage } from '../pages/checkout-invoice/checkout-invoice';
import { OrderProvider } from '../providers/order/order';
import { OrderDetailsPage } from '../pages/order-details/order-details';
import { ChangeStatusPage } from '../pages/change-status/change-status';
import { ImageInvoicePage } from '../pages/image-invoice/image-invoice';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ProfilePage,
    RegisterPage,
    LandingPage,
    BidsPage,
    OrdersPage,
    ViewRequestPage,
    Register_2Page,
    ActivebidsPage,
    CheckoutInvoicePage,
    OrderDetailsPage,
    ChangeStatusPage,
    ImageInvoicePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ProfilePage,
    RegisterPage,
    LandingPage,
    BidsPage,
    OrdersPage,
    ViewRequestPage,
    Register_2Page,
    ActivebidsPage,
    CheckoutInvoicePage,
    OrderDetailsPage,
    ChangeStatusPage,
    ImageInvoicePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    NativeGeocoder,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    BiddingProvider,
    OrderProvider
  ]
})
export class AppModule {}
