
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import {CONFIG} from '../../../config/config';
import { AuthProvider } from '../auth/auth';

/*
  Generated class for the BiddingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BiddingProvider {

  public notifToken: any;
  public token: any;
  public API_URL = CONFIG['API_URL'];

  constructor(public http: Http, public authService: AuthProvider) {
    this.token = authService.token
    console.log('Hello BiddingProvider Provider');
  }

  pharmacistsGetUnseenBids(){
      return new Promise((resolve,reject) => {
        let headers = new Headers();
        headers.append('Authorization',this.token);
        headers.append('Content-type','application/json');

        this.http.post(this.API_URL+"/pharmacist/bids/unseen",JSON.stringify({}),{headers:headers})
        .subscribe((res) => {
          resolve(res.json());
        }, (err) => {
          reject(err)
        })
      })
  }

  pharmacistGetActiveBids(){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization',this.token);
      headers.append('Content-type','application/json');

      this.http.post(this.API_URL+"/pharmacist/bids/active",JSON.stringify({}),{headers:headers})
      .subscribe((res) => {
        resolve(res.json())
      }, (err) => {
        reject(err)
      })
    })    
  }

  pharmacistAccept(details){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization',this.token);
      headers.append('Content-type','application/json');

      this.http.post(this.API_URL+"/pharmacist/bid/accept",JSON.stringify(details),{headers:headers})
      .subscribe((res) => {
        resolve(res.json())
      }, (err) => {
        reject(err)
      })
    })   
  }

  pharmacistAcceptImage(details){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Authorization',this.token);
      headers.append('Content-type','application/json');

      this.http.post(this.API_URL+"/pharmacist/bid/image/accept",JSON.stringify(details),{headers:headers})
      .subscribe((res) => {
        resolve(res.json())
      }, (err) => {
        reject(err)
      })
    })   

  }
}
