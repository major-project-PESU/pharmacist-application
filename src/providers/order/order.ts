import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {CONFIG} from '../../../config/config';
import { AuthProvider } from '../auth/auth';
/*
  Generated class for the OrderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrderProvider {
  public token: any;
  public API_URL = CONFIG['API_URL'];

  constructor(public http: Http, public authServ:AuthProvider) {
    this.token = this.authServ.token;
    console.log('Hello OrderProvider Provider');
  }

  pharmacistGetAllOrders(){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append("Authorization", this.token);
      headers.append('Content-type','application/json');

      this.http.post(this.API_URL+"/pharmacist/orders/all",JSON.stringify({}),{headers:headers})
      .subscribe(res => {
        resolve(res.json());
      }, err => {
        reject(err)
      })
    })
  }

  pharmacistChangeStatus(details){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append("Authorization", this.token);
      headers.append('Content-type','application/json');

      this.http.post(this.API_URL+"/pharmacist/order/changestatus",JSON.stringify(details),{headers:headers})
      .subscribe(res => {
        resolve(res.json());
      }, err => {
        reject(err)
      })
    })
  }


}
