import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LandingPage } from '../landing/landing';
import { RegisterPage } from '../register/register';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email:String;
  password:String;
  constructor(public navCtrl: NavController, public navParams: NavParams, public authServ:AuthProvider) {
  }

  ionViewDidLoad() {
    this.authServ.checkAuthentication().then((res) => {
      this.navCtrl.setRoot(LandingPage)
    }, err => {
      console.log(err)
    })
    console.log('ionViewDidLoad LoginPage');
  }

  login(){
    console.log(this.email,this.password)
    let json = {
      email:this.email,
      password:this.password
    }
    this.authServ.login(json).then((res) => {
      this.navCtrl.setRoot(LandingPage);

    }, (err) => {
      console.log(err)
    })
  }

  goToRegisterPage(){
    this.navCtrl.push(RegisterPage);
  }
}

