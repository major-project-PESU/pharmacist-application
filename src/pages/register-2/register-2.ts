import {
  Component,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
} from 'ionic-angular';
import {
  Geolocation
} from '@ionic-native/geolocation/ngx';
// import {GoogleMapsEvent} from '@ionic-native/goog'
import {
  NativeGeocoder,
  NativeGeocoderReverseResult,
  NativeGeocoderForwardResult
} from '@ionic-native/native-geocoder/ngx';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';
/**
 * Generated class for the Register_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-2',
  templateUrl: 'register-2.html',
})
export class Register_2Page {

  @ViewChild('map') mapElement: ElementRef;
  
  map: any;
  public lat: any;
  public lng: any;
  public address: any;
  public city: any;
  public locality: any;
  public postalCode:any;
  public marker = null;
  public data:any;
  public geocoder = new google.maps.Geocoder();


  constructor(public navCtrl: NavController, public navParams: NavParams,public authServ:AuthProvider, public geolocation: Geolocation, private _GEOCODE: NativeGeocoder) {  
    // Register_2Page['address'] = "abc;"
    this.data = this.navParams.get('data');
  }

  setLocation() {
    let options = {
      timeout: 30000,
      enableHighAccuracy: true
    }
    this.geolocation.getCurrentPosition(options).then((resp) => {
      this.lat = (resp.coords.latitude);
      this.lng = (resp.coords.longitude);
      var self = this;
      let latLng = new google.maps.LatLng(this.lat, this.lng);
      if(this.marker){
        this.marker.setPosition(latLng);
      }
      else{
        this.marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: latLng,
          draggable: true
        });
        google.maps.event.addListener(this.marker, 'dragend', () => {
          this.lat = this.marker.getPosition().lat();
          this.lng = this.marker.getPosition().lng();
          // console.log(this.lat,this.lng);
          this.geocoder.geocode({
            "location": new google.maps.LatLng(this.lat,this.lng)
          }, function (res, status) {
            if(status.toString() != "OK"){
              self.address = "Nada"
            }
            self.address = (res[0].formatted_address);
            console.log("Address;")
            console.log(res);
          })
          this._GEOCODE.reverseGeocode(this.lat, this.lng).then((res) => {
            this.city = res[0].locality;
            this.locality = res[0].subLocality;
            this.postalCode = res[0].postalCode
          }, (err) => {
            console.log(err)
          })
        });
      }
      this.map.setCenter(latLng);
      this.geocoder.geocode({
        "location": latLng
      }, function (res, status) {
        if(status.toString() != "OK"){
          self.address = "Nada"
        }
        self.address = (res[0].formatted_address);
      })
      this._GEOCODE.reverseGeocode(this.lat, this.lng).then((res) => {
        this.city = res[0].locality;
        this.locality = res[0].subLocality;
        this.postalCode = res[0].postalCode
      }, (err) => {
        console.log(err)
      });
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  loadMap() {

    let latLng = new google.maps.LatLng(-34.9290, 138.6010);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.setLocation()
    console.log(this.address);

  }


  submit(){
    this.data['address'] = {
      fullAddress:this.address,
      city:this.city,
      locality:this.locality,
      postalCode:this.postalCode,
      lat:this.lat,
      lng : this.lng
    }
    this.authServ.createAccount(this.data).then((res) => {
      this.navCtrl.push(LoginPage)
    }, (err) => {
      console.log(err)
    })
  }
  ionViewDidEnter() {
    this.loadMap();

  }

}
