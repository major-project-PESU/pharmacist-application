import {
  Component
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
} from 'ionic-angular';
import {
  FormBuilder,
  FormArray,
  FormGroup,
  Validators
} from '@angular/forms';
/**
 * Generated class for the ViewRequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-request',
  templateUrl: 'view-request.html',
})
export class ViewRequestPage {
  public form: any;
  public total = 0;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private _FB: FormBuilder) {

    this.form = this._FB.group({
      medicines: this._FB.array([
        this.initMedicinesField()
      ])
    });
  }

  initMedicinesField(): FormGroup {
    return this._FB.group({
      name: ['', Validators.required],
      alternativeFor: [''],
      qty: ['', Validators.required],
      pricePerUnit: ['', Validators.required],
      subTotal: ['']
    });
  }

  calculateTotal(val){
    console.log("VALUE")
    console.log(val.medicines[i])
    for(var i in val){
      this.total += val.medicines[i].subTotal;
    }
  }

  addNewInputField(): void {
    const control = < FormArray > this.form.controls.medicines;
    control.push(this.initMedicinesField());
  }

  removeInputField(i: number): void {
    const control = < FormArray > this.form.controls.medicines;
    control.removeAt(i);
  }

  manage(val: any): void {
    console.dir(val);
  }

  addMedicine() {

  }
}
