import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BiddingProvider } from '../../providers/bidding/bidding';
import { CheckoutInvoicePage } from '../checkout-invoice/checkout-invoice';
import { ImageInvoicePage } from '../image-invoice/image-invoice';

/**
 * Generated class for the BidsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bids',
  templateUrl: 'bids.html',
})
export class BidsPage {

  public isToggled = false;
  public unseen : any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public bidServ:BiddingProvider) {
  }


  ionViewDidEnter(){
    this.bidServ.pharmacistsGetUnseenBids().then((res) => {
      console.log(res)
      this.unseen = res;
    }, (err) => {
      console.log("Error Getting Active Bids: "+err)
    });
  }

  goToCheckoutInvoicePage(bid){
    if(bid.url){
      this.navCtrl.push(ImageInvoicePage,{bid:bid})
    }else{
      this.navCtrl.push(CheckoutInvoicePage,{bid:bid})
    }
  }

  rejectBid(){

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad BidsPage');
  }

}
