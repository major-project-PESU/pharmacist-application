import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { BiddingProvider } from '../../providers/bidding/bidding';
import { LandingPage } from '../landing/landing';
import * as moment from 'moment';

/**
 * Generated class for the CheckoutInvoicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout-invoice',
  templateUrl: 'checkout-invoice.html',
})
export class CheckoutInvoicePage {

  public bid :any;
  public dispatchDate:any;
  public dispatchTime:any;
  public delivery = false;
  public valid = false;
  public today = moment();
  public date = this.today.format('YYYY-MM-DD');
  public time = this.today.format('hh:mm a');
  public medicines : any = [];
  constructor(public navCtrl: NavController, public alertCtrl: AlertController,public navParams: NavParams, public bidServ : BiddingProvider) {
    this.bid = this.navParams.get('bid');
    this.medicines = this.bid['medicines'];
  }
  

  isValid(){
    if(this.dispatchDate && this.dispatchTime && this.bid.total){
      this.valid = true;
    }
  }
  showSelectedTimeBeforePresentTimeAlert(){
    let alert = this.alertCtrl.create({
      title:"Delivery Time has passed",
      message:"The delivery time indicated is before the present time, please correct the time",
      buttons : [{
        text:"OK",
        role:'cancel'
      }]
    });
    alert.present();
  }

  confirmBid(){
    let alert = this.alertCtrl.create({
      title: "Confirm Bid",
      message: "Do you want to place the bid with the existing information?",
      buttons : [
        {
          text:"Yes",
          handler: () => {
            this.accept();
          }
        },
        {
          text:"No",
          role:'cancel'
        }
      ]
    });
    alert.present();
  }
  accept(){
    let dispatchDateTime = this.dispatchDate + " " + this.dispatchTime;
    let d = moment(dispatchDateTime);
    console.log(this.dispatchDate,this.dispatchTime,this.delivery)
    // return 21;
    if(d.isBefore(moment())){
      this.showSelectedTimeBeforePresentTimeAlert()
      return 1
      // Add error alert
    }
    let json = {
      "bidID":this.bid['_id'],
      "total":this.bid.total,
      "dispatchDate":this.dispatchDate,
      "dispatchTime":this.dispatchTime,
      "delivery":this.delivery,
    }
    this.bidServ.pharmacistAccept(json).then((res) => {
      this.navCtrl.setRoot(LandingPage);
    }, (err) => {
      console.log(err)
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutInvoicePage');
  }

}
