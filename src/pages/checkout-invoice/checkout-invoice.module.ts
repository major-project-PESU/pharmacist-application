import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckoutInvoicePage } from './checkout-invoice';

@NgModule({
  declarations: [
    CheckoutInvoicePage,
  ],
  imports: [
    IonicPageModule.forChild(CheckoutInvoicePage),
  ],
})
export class CheckoutInvoicePageModule {}
