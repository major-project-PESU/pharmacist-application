import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OrderProvider } from '../../providers/order/order';

/**
 * Generated class for the ChangeStatusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-status',
  templateUrl: 'change-status.html',
})
export class ChangeStatusPage {

  public delivered = false;
  public message:any;
  public order:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public orderServ:OrderProvider) {
    this.order = this.navParams.get('order')
  }

  submit(){
    let json = {
      orderID:this.order['_id'],
      message:this.message,
      delivered:this.delivered
    }
    this.orderServ.pharmacistChangeStatus(json).then((res) => {
      console.log(res);
      this.navCtrl.pop()
    }, err => {
      console.log(err)
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangeStatusPage');
  }

}
