import { Component  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';
import { Register_2Page } from '../register-2/register-2';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  public form:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private _FB          : FormBuilder) {
      this.form = this._FB.group({
        name: [''],
        email: [''],
        password: [''],
        contact:['']
     });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');

  }


  register(val){
    console.log(val)
    this.navCtrl.push(Register_2Page,{data:val});
  }

}
