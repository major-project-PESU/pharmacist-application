import {
  Component
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';
import {
  FormBuilder,
  FormArray,
  FormGroup,
  Validators
} from '@angular/forms';
import { BiddingProvider } from '../../providers/bidding/bidding';
import { LandingPage } from '../landing/landing';
import * as moment from 'moment';

/**
 * Generated class for the ImageInvoicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-image-invoice',
  templateUrl: 'image-invoice.html',
})
export class ImageInvoicePage {

  public bid: any;
  public form: FormGroup;
  public dispatchDate:any;
  public dispatchTime:any;
  public delivery = false;
  public valid = false;
  public today = moment();
  public date = this.today.format('YYYY-MM-DD');
  public time = this.today.format('hh:mm a');

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, private _FB: FormBuilder, public navParams: NavParams, public biddingServ:BiddingProvider) {
    this.bid = this.navParams.get('bid');
    console.log(this.bid)

    this.form = this._FB.group({
      medicines: this._FB.array([
        this.initTechnologyFields()
      ])
    })
  }
   isValid(){
    if(this.dispatchDate && this.dispatchTime){
      this.valid = true;
    }
   }

  initTechnologyFields(): FormGroup {
    return this._FB.group({
      Name: ['', Validators.required],
      Price:['',Validators.required],
      Units:['',Validators.required],
      AlternativeFor:['']  
    });
  }
  addNewInputField(): void {
    const control = < FormArray > this.form.controls.medicines;
    control.push(this.initTechnologyFields());
  }
  removeInputField(i: number): void {
    const control = < FormArray > this.form.controls.medicines;
    control.removeAt(i);
  }

  showSelectedTimeBeforePresentTimeAlert(){
    let alert = this.alertCtrl.create({
      title:"Delivery Time has passed",
      message:"The delivery time indicated is before the present time, please correct the time",
      buttons : [{
        text:"OK",
        role:'cancel'
      }]
    });
    alert.present();
  }

  confirmBid(val){
    let alert = this.alertCtrl.create({
      title: "Confirm Bid",
      message: "Do you want to place the bid with the existing information?",
      buttons : [
        {
          text:"Yes",
          handler: () => {
            this.manage(val);
          }
        },
        {
          text:"No",
          role:'cancel'
        }
      ]
    });
    alert.present();
  }
  manage(val: any) {
    let dispatchDateTime = this.dispatchDate + " " + this.dispatchTime;
    let d = moment(dispatchDateTime);
    if(d.isBefore(moment())){
      this.showSelectedTimeBeforePresentTimeAlert()
      return 1;
    }
    val.total = 0;
    val.bidID = this.bid._id;
    val.dispatchDate = this.dispatchDate;
    val.dispatchTime = this.dispatchTime;
    val.delivery=this.delivery;
    for(var i in val.medicines){
      var temp = val.medicines[i];
      val.medicines[i]['Total'] = ((parseFloat(temp['Units']) * parseFloat(temp['Price'])).toFixed(2))
      val.total += parseFloat(val.medicines[i]['Total'])
    }
    this.biddingServ.pharmacistAcceptImage(val).then((res) => {
      this.navCtrl.setRoot(LandingPage)
    }, err => {
      console.log(err)
    })
    console.dir(val);

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ImageInvoicePage');
  }

}
