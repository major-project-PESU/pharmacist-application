import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImageInvoicePage } from './image-invoice';

@NgModule({
  declarations: [
    ImageInvoicePage,
  ],
  imports: [
    IonicPageModule.forChild(ImageInvoicePage),
  ],
})
export class ImageInvoicePageModule {}
