import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfilePage } from '../profile/profile';
import { OrdersPage } from '../orders/orders';
import { HomePage } from '../home/home';
import { ActionSheetController } from 'ionic-angular';
import { BidsPage } from '../bids/bids';
import { ActivebidsPage } from '../activebids/activebids';

/**
 * Generated class for the LandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {

  homePage:any;
  profilePage:any;
  bidsPage:any;
  ordersPage:any
  activeBidsPage:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController
    ) {
    this.homePage = HomePage;
    this.profilePage = ProfilePage;
    this.bidsPage = BidsPage;
    this.ordersPage = OrdersPage;
    this.activeBidsPage = ActivebidsPage
  }
 
}
