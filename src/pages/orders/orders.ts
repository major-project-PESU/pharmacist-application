import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { OrderProvider } from '../../providers/order/order';
import { OrderDetailsPage } from '../order-details/order-details';
import { ChangeStatusPage } from '../change-status/change-status';

/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {

  public filterPrescriptionBy = 1;
  public orders : any = [];
  public allOrders : any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public orderServ:OrderProvider, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrdersPage');
  }

  ionViewDidEnter(){
    this.getOrders();

  }

  goToOrderDetails(request){
    this.navCtrl.push(OrderDetailsPage,{request:request});
  }

  changeStatus(order){
    const modal = this.modalCtrl.create(ChangeStatusPage, {order:order});
    modal.present();
  }

  getOrders(){
    this.orderServ.pharmacistGetAllOrders().then(res => {
      console.log(res)
      this.allOrders = res;
      this.filter();
      // this.orders = res;
    }, err => {
      console.log(err)
    })
  }
  filter(){
    function typeCompare(type){
      return function(element){
        return element.status === parseInt(type);
      }
    }
    this.orders = this.allOrders.filter(typeCompare(this.filterPrescriptionBy))
  }

}
