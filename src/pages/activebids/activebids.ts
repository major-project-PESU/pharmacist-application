import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BiddingProvider } from '../../providers/bidding/bidding';

/**
 * Generated class for the ActivebidsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-activebids',
  templateUrl: 'activebids.html',
})
export class ActivebidsPage {

  public active : any = []
  constructor(public navCtrl: NavController, public navParams: NavParams, public bidServ:BiddingProvider) {
  }

  ionViewDidEnter(){
    this.bidServ.pharmacistGetActiveBids().then((res) => {
      console.log(res)
      this.active = res;
    }, (err) => {
      console.log("Error getting Unseen Bids: "+err)
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ActivebidsPage');
  }

}
