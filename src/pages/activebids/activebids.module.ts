import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivebidsPage } from './activebids';

@NgModule({
  declarations: [
    ActivebidsPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivebidsPage),
  ],
})
export class ActivebidsPageModule {}
