* Add total option in proforma invoice
* Add View Proforma Invoice before submission, going back should edit the form.

## Registration 

* Run function everytime marker in registration map is moved - DONE
* Set Location - DONE
* Add City as a separate option in address (see e-commerce websites) - DONE
* Full Address does not get updated when marker is moved - DONE
